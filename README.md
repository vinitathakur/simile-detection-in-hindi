# Simile Detection in Hindi

## Introduction
* Simile detection enables readers of prose, poetry, religious preachings, political speeches, news articles etc. to derive correct meaning of their readings by understanding the explicit comparisons being made between two or more ideas/objects/concepts or any such entities.
* Unlike English, where it is highly probable that a sentence can be classified as a simile if two clauses are combined with the help of ‘like’, ‘just like’, ‘as’, ‘such as’ (and variants of these comparison operators), Hindi has a liberal way of tackling Similes. 
* It does not restrict the domain of simile as merely the presence of तरह/tarah/like, इस तरह/is tarah/like this, उस तरह/us tarah/like that, जैसे की/jaise ki/such as (and variants of these comparison operators). 
* The structure of a sentence will dominate its implication and hence Hindi language opens a broad scope for detecting similes which largely depends on structural nuances.
* Another aspect that needs attention is the fact that Hindi is a free word order language indicating that exploiting dependency parsing holds utmost significance to derive the essence of the sentence.
* It is interesting to note that extensive research and work has been done previously on metaphor detection[1] (implicit comparison) using conditional random fields, neural networks and other machine learning algorithms. Our proposed approach for identifying explicit comparison in Hindi takes into consideration the structure of the sentences resulting in an intuitive approach of categorizing it as a simile.

## Instructions for running the code

* The ```main.py``` file is the starting point of the application
* All the Input/Output files are stored in the ```I/O files``` folder
* There is an Error Analysis file that has results that show the progression and research in the project
* The hindi depedency parser is a separate module that is present in the folder ```hindi-dependency-parser-2.0```. Running this parser produces a ```hindi.output``` file