import math


"""This function reads the model file and
returns a dictionary with the token as key and
value as a list of positive and negative class probabilities"""
def read_model(model_filename):
    content = open(model_filename, encoding = 'utf-8').read()
    list_sep = content.split('\n')
    list_sep.remove(list_sep[len(list_sep) - 1])

    token_dict = {}
    for element in list_sep:
        list_content = element.split(' ')
        if list_content.__contains__(''):
            list_content.remove('')
        token_dict[list_content[0]] = [list_content[1], list_content[2]]

    return token_dict


"""This function reads the test data and cleans it by
removing stopwords and punctuation"""
def read_test_data(testdata_filename, stop_word_list):
    final_list = []
    list_fin = [list.rstrip('\n') for list in open(testdata_filename, encoding="utf-8")]
    for sentence in list_fin:
        temp_list = sentence.split(' ')
        temp_list = list(set(temp_list) - set(stop_word_list))
        temp_list = [element.lower() for element in temp_list]
        temp_list = list(filter(None, temp_list))
        final_list.append(temp_list)

    return final_list


"""This function gets the probability of a
particular token from a dictionary"""
def get_probability(token_dict, token, classification):
    if token not in token_dict:
        return 1.0
    elif classification == 'SIMILE':
        return token_dict[token][0]
    elif classification == 'NOT':
        return token_dict[token][1]


"""This function builds a probability matrix
that stores the probability of each sentence
for each class"""
def build_probability_matrix(final_list, token_dict):
    l = len(final_list)
    w = 2
    review_classification = [[0 for x in range(w)] for y in range(l)]
    simile_probability = 0
    non_simile_probability = 0
    index = 0
    for sentence in final_list:
        for word in sentence:
            simile_probability += math.log(float(get_probability(token_dict, word, 'SIMILE')), 10)
            non_simile_probability += math.log(float(get_probability(token_dict, word, 'NOT')), 10)
        review_classification[index][0] = simile_probability
        review_classification[index][1] = non_simile_probability
        m = 0
        n = 0
        index += 1
    return review_classification


"""This function writes classification output
to the specified filename"""
def write_output_to_file(review_classification, output_filename):
    out_file = open(output_filename, 'w',encoding= 'utf-8')
    for i in range(len(review_classification)):
        if review_classification[i][0] > review_classification[i][1]:
            out_file.write('SIMILE'.strip())
        else:
            out_file.write('NOT'.strip())
        out_file.write('\n')
    out_file.close()
