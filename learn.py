# coding:UTF-8

"""This function returns a list of lists where
 each list contains tokens"""
def data_cleaning(trainData, stop_word_list, punctuation_list):
    training_token_list = []
    # separate review with next line character
    sentence_list = [list.rstrip('\n') for list in open(trainData, encoding="utf-8-sig")]
    # convert all elements to lowercase, remove stopwords, remove punctuations, filter null values
    for sentence in sentence_list:
        token_list = sentence.split(' ')
        token_list = [element.lower() for element in token_list]
        for token in range(len(token_list)):
            token_list[token] = ''.join(ch for ch in token_list[token] if ch not in punctuation_list)
        token_list = list(set(token_list) - set(stop_word_list))
        token_list = list(filter(None, token_list))
        training_token_list.append(token_list)

    return training_token_list


"""This function creates a list of unique tokens from the given data"""
def create_list_of_tokens(training_token_list):
    list_of_tokens = []
    for list in training_token_list:
        for i in list:
            if i not in list_of_tokens:
                list_of_tokens.append(i)
    return list_of_tokens


"""This function returns a dictionary containing
positive and negative prior probabilities"""
def calculate_priors(list_of_labels):
    pos1 = 0
    neg1 = 0
    for c in list_of_labels:
        if c == 'SIMILE':
            pos1 += 1
        else:
            neg1 += 1

    prior = {'p': pos1 / float(pos1 + neg1), 'n': neg1 / float(pos1 + neg1)}
    return prior


"""function to calculate occurences of a
particular token in a particular class
initializing dict with [1,1] for add-one smoothing"""
def calculate_probability(training_token_list, list_of_labels):
    i = -1
    token_probabilities = dict()
    for token_list in training_token_list:
        i += 1
        for token in token_list:
            if token in token_probabilities:
                if list_of_labels[i] == 'SIMILE':
                    token_probabilities[token][0] += 1
                else:
                    token_probabilities[token][1] += 1

            else:
                token_probabilities[token] = [1, 1]

                if list_of_labels[i] == 'SIMILE':
                    token_probabilities[token][0] += 1
                else:
                    token_probabilities[token][1] += 1

    pos_denominator = 0
    neg_denominator = 0

    for key, value in token_probabilities.items():
        pos_denominator += value[0]
        neg_denominator += value[1]

    for key, val in token_probabilities.items():
        token_probabilities[key][0] /= float(pos_denominator)
        token_probabilities[key][1] /= float(neg_denominator)

    return token_probabilities


"""This method writes the model to a file"""
def write_model_to_file(token_probabilities, filename):
    ff = open(filename, 'w', encoding='utf-8-sig')
    for token in token_probabilities:
        ff.write(token)
        ff.write(' ')
        ff.write(str(token_probabilities[token][0]))
        ff.write(' ')
        ff.write(str(token_probabilities[token][1]))
        ff.write('\n')