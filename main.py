# coding:UTF-8
import learn
import classify
import update_labels

# defining filenames
labelData = 'IO files/sent_sim.txt'
trainData = 'IO files/correct sentences.txt'
testdata_filename = 'IO files/test_correct_sentences.txt'
model_filename = 'IO files/nbmodel.txt'
output_filename = 'IO files/nboutput.txt'
output_final_filename = 'IO files/final_output.txt'
test_sentences_filename = 'IO files/test_correct_sentences.txt'
parser_ouput_file = 'IO files/hindi.output'

# defining lists
stop_word_list = ['के', 'का', 'एक', 'में', 'है', 'यह', 'और', 'से', 'हैं', 'को', 'पर', 'इस', 'होता', 'कि', 'जो',
                    'कर', 'मे', 'गया', 'करने', 'किया', 'लिये', 'अपने', 'ने', 'बनी', 'नहीं', 'तो', 'ही', 'या', 'एवं',
                    'दिया', 'हो', 'इसका', 'था', 'द्वारा', 'हुआ', 'तक', 'साथ', 'करना', 'वाले', 'बाद', 'लिए', 'आप',
                    'कुछ', 'सकते', 'किसी', 'ये', 'इसके', 'सबसे', 'इसमें', 'थे', 'दो', 'होने', 'वह', 'वे', 'करते',
                    'बहुत',
                    'कहा', 'वर्ग', 'कई', 'करें', 'होती', 'अपनी', 'उनके', 'थी', 'यदि', 'हुई', 'जा', 'ना', 'इसे',
                    'कहते',
                    'जब',
                    'होते', 'कोई', 'हुए', 'व', 'न', 'अभी', 'जैसे', 'सभी', 'करता', 'उनकी', 'उस', 'आदि', 'कुल', 'एस',
                    'रहा',
                    'इसकी', 'सकता', 'रहे', 'उनका', 'इसी', 'रखें', 'अपना', 'पे', 'उसके']
punctuation_list = {';', '~', '+', "'", ',', '.', '%', '_', '[', '{', '`', '<', '\\', '=', '*', '!', '/', '}',
                    '"', ':', '@', ')', ']', '$', '?', '(', '#', '-', '|', '&', '>', '^', '‘', '’', '”', '“', '', '।'}

if __name__ == "__main__":
    list_of_labels = [list.rstrip('\n') for list in open(labelData, 'r')]

    #Learning
    training_token_list = learn.data_cleaning(trainData, stop_word_list, punctuation_list)
    list_of_tokens = learn.create_list_of_tokens(training_token_list)
    priors = learn.calculate_priors(list_of_labels)
    token_probabilities = learn.calculate_probability(training_token_list, list_of_labels)
    learn.write_model_to_file(token_probabilities, model_filename)

    #Classification
    token_dict = classify.read_model(model_filename)
    final_list = classify.read_test_data(testdata_filename, stop_word_list)
    review_classification = classify.build_probability_matrix(final_list, token_dict)
    classify.write_output_to_file(review_classification, output_filename)

    #Update classsification
    label_list = [list.rstrip('\n') for list in open(output_filename, 'r', encoding="utf-8-sig")]
    sentence_word_list = update_labels.read_test_sentences(test_sentences_filename)
    listword, listtag, listnum = update_labels.read_parser_output(parser_ouput_file)
    updated_label_list = update_labels.change_tags(sentence_word_list, listtag, listnum, label_list)
    update_labels.write_tags_to_output_file(updated_label_list, output_final_filename)
